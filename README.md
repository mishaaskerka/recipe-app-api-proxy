# Recipe App API Proxy

NGINX proxy app for our recipe app API 

## Usage

### Environement Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname for the app to forward requests to (defualt: `app`)
* `APP_PORT` - Port for the app to forward requests to (defualt: `9000`)

